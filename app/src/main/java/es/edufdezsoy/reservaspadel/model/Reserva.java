package es.edufdezsoy.reservaspadel.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Reserva implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("start")
    private String desde;
    @SerializedName("end")
    private String hasta;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("user_name")
    private String user_name;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getDesde() {
        return desde;
    }
    public void setDesde(String desde) {
        this.desde = desde;
    }
    public String getHasta() {
        return hasta;
    }
    public void setHasta(String hasta) {
        this.hasta = hasta;
    }
    public int getUser_id() {
        return user_id;
    }
    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }
    public String getUser_name() {
        return user_name;
    }
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Reserva(int id, String desde, String hasta, int user_id, String user_name) {
        this.id = id;
        this.desde = desde;
        this.hasta = hasta;
        this.user_id = user_id;
        this.user_name = user_name;
    }

    public Reserva(int id, String desde, String hasta, int user_id) {
        super();
        this.id = id;
        this.desde = desde;
        this.hasta = hasta;
        this.user_id = user_id;
    }

    public Reserva(String desde, String hasta, int user_id, String user_name) {
        this.desde = desde;
        this.hasta = hasta;
        this.user_id = user_id;
        this.user_name = user_name;
    }

    public Reserva(String desde, String hasta, int user_id) {
        super();
        this.desde = desde;
        this.hasta = hasta;
        this.user_id = user_id;
    }

    public Reserva() {}

    @Override
    public String toString() {
        return  desde + '\n' + hasta + '\n' + user_id;
    }
}
