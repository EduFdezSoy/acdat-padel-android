package es.edufdezsoy.reservaspadel.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import es.edufdezsoy.reservaspadel.R;
import es.edufdezsoy.reservaspadel.model.Reserva;

/**
 * Created by paco on 6/02/18.
 */

public class ReservasAdapter extends RecyclerView.Adapter<ReservasAdapter.ViewHolder> {
    private ArrayList<Reserva> reservas;

    public ReservasAdapter() {
        this.reservas = new ArrayList<>();
    }

    public void setRepos(ArrayList<Reserva> repos) {
        reservas = repos;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView linea1, linea2;

        public ViewHolder(View itemView) {
            super(itemView);
            linea1 = itemView.findViewById(R.id.textView1);
            linea2 = itemView.findViewById(R.id.textView2);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();

        // Inflate the custom layout
        LayoutInflater inflater = LayoutInflater.from(context);
        View siteView = inflater.inflate(R.layout.item_reserva, parent, false);

        // Return a new holder instance
        return new ViewHolder(siteView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Reserva reserva = reservas.get(position);

        try {
            holder.linea1.setText(reserva.getDesde() + " - " + reserva.getHasta());
            holder.linea2.setText("Reservado por: " + reserva.getUser_name());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return reservas.size();
    }

    public int getId(int position) {

        return this.reservas.get(position).getId();
    }

    public Reserva getAt(int position) {
        return this.reservas.get(position);
    }

    public void add(Reserva reserva) {
        this.reservas.add(reserva);
        notifyItemInserted(reservas.size() - 1);
        notifyItemRangeChanged(0, reservas.size() - 1);
    }

    public void modifyAt(Reserva reserva, int position) {
        this.reservas.set(position, reserva);
        notifyItemChanged(position);
    }

    public void removeAt(int position) {
        this.reservas.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(0, reservas.size() - 1);
    }
}
