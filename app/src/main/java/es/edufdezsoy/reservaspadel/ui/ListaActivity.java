package es.edufdezsoy.reservaspadel.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import es.edufdezsoy.reservaspadel.R;
import es.edufdezsoy.reservaspadel.adapter.ClickListener;
import es.edufdezsoy.reservaspadel.adapter.RecyclerTouchListener;
import es.edufdezsoy.reservaspadel.adapter.ReservasAdapter;
import es.edufdezsoy.reservaspadel.model.Reserva;
import es.edufdezsoy.reservaspadel.network.ApiTokenRestClient;
import es.edufdezsoy.reservaspadel.utils.SharedPreferencesManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaActivity extends AppCompatActivity {
    public static final int ADD_CODE = 2;
    public static final int UPDATE_CODE = 3;

    int positionClicked;
    RecyclerView recyclerView;
    FloatingActionButton floatingActionButton;
    SharedPreferencesManager preferencesManager;
    ReservasAdapter adapter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        recyclerView = findViewById(R.id.recycler);
        floatingActionButton = findViewById(R.id.floatingButton);

        preferencesManager = new SharedPreferencesManager(this);
        // Log.v("edu", preferencesManager.getToken());

        adapter = new ReservasAdapter();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Intent intent = new Intent(ListaActivity.this, EditarActivity.class);
                intent.putExtra("reserva", adapter.getAt(position));
                startActivityForResult(intent, UPDATE_CODE);
                positionClicked = position;
            }

            @Override
            public void onLongClick(View view, int position) {
                confirm(adapter.getAt(position).getId(), adapter.getAt(position).getDesde(), position);
            }
        }));

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListaActivity.this, ReservarActivity.class);
                startActivityForResult(i, ADD_CODE);
            }
        });

        ApiTokenRestClient.deleteInstance();
        downloadReservas();
    }

    private void downloadReservas() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Conectando...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<ArrayList<Reserva>> call = ApiTokenRestClient.getInstance(preferencesManager.getToken()).getReservas();
        call.enqueue(new Callback<ArrayList<Reserva>>() {
            @Override
            public void onResponse(Call<ArrayList<Reserva>> call, Response<ArrayList<Reserva>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    adapter.setRepos(response.body());
                    showMessage("Reservas descargadas");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Error: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Reserva>> call, Throwable t) {
                progressDialog.dismiss();
                showMessage("Error al conectar: " + t.getMessage());
            }
        });
    }

    private void confirm(final int idReserva, String name, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(name + "\nDesea borrar la reserva?")
                .setTitle("Delete")
                .setPositiveButton("Borrar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        connection(position);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    private void connection(final int position) {
        Call<ResponseBody> call = ApiTokenRestClient.getInstance(preferencesManager.getToken()).deleteReserva(adapter.getId(position));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Conectando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    adapter.removeAt(position);
                    showMessage("Reserva borrada");
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Error borrando la reserva: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    showMessage(message.toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                showMessage("Error al conectar: " + t.getMessage());
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Reserva reserva = new Reserva();

        if (requestCode == ADD_CODE)
            if (resultCode == RESULT_OK) {
                reserva = (Reserva) getIntent().getSerializableExtra("reserva");
                adapter.add(reserva);
            }

        if (requestCode == UPDATE_CODE)
            if (resultCode == RESULT_OK) {
                reserva = (Reserva) getIntent().getSerializableExtra("reserva");
                adapter.modifyAt(reserva, positionClicked);
            }
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }
}
