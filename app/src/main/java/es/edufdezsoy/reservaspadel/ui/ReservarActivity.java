package es.edufdezsoy.reservaspadel.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import es.edufdezsoy.reservaspadel.R;
import es.edufdezsoy.reservaspadel.model.Reserva;
import es.edufdezsoy.reservaspadel.network.ApiTokenRestClient;
import es.edufdezsoy.reservaspadel.utils.SharedPreferencesManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReservarActivity extends AppCompatActivity {
    TextView error;
    EditText fecha;
    Spinner desde, hasta;
    Button guardar;
    ProgressDialog progressDialog;
    SharedPreferencesManager preferencesManager;
    SimpleDateFormat dateFormatFrom;
    SimpleDateFormat dateFormatTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservar);

        preferencesManager = new SharedPreferencesManager(this);
        dateFormatFrom = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatTo = new SimpleDateFormat("dd/MM/yyyy");

        error = findViewById(R.id.textViewError);
        fecha = findViewById(R.id.editTextFecha);
        desde = findViewById(R.id.spinnerDesde);
        hasta = findViewById(R.id.spinnerHasta);
        guardar = findViewById(R.id.button);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveClick(v);
            }
        });
    }

    private void onSaveClick(View v) {
        if (desde.getSelectedItemPosition() > hasta.getSelectedItemPosition()) {
            error.setText("comprueba las horas, no puedes reservar la pista asi");
            return;
        }
        if (hasta.getSelectedItemPosition() > desde.getSelectedItemPosition() + 2) {
            error.setText("solo puedes reservar un maximo de dos horas");
            return;
        }

        Date ahora = new Date();
        Date reservaNueva = null;
        try {
            reservaNueva = dateFormatTo.parse(fecha.getText().toString() + " " + (desde.getSelectedItemPosition() + 9) + ":00");
        } catch (ParseException e) {
            error.setText("Formato de fecha erroneo, usa dd/MM/yyyy");
            return;
        }
        if (reservaNueva.before(ahora)) {
            error.setText("No puedes reservar la pista en el pasado!");
            return;
        }

        Reserva reserva = new Reserva();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        reserva.setDesde(simpleDateFormat.format(reservaNueva));
        try {
            reservaNueva = dateFormatTo.parse(fecha.getText().toString() + " " + (hasta.getSelectedItemPosition() + 9) + ":00");
        } catch (ParseException e) {
            e.printStackTrace();
            error.setText("error inexperado");
            return;
        }
        reserva.setHasta(simpleDateFormat.format(reservaNueva));
        error.setText("");
        guardarReserva(reserva);
    }

    private void guardarReserva(final Reserva reserva) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Connecting . . .");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<Reserva> call = ApiTokenRestClient.getInstance(preferencesManager.getToken()).createReserva(reserva);
        call.enqueue(new Callback<Reserva>() {
            @Override
            public void onResponse(Call<Reserva> call, Response<Reserva> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Reserva reserva1 = response.body();
                    Intent i = new Intent();
                    i.putExtra("reserva", reserva1);
                    setResult(RESULT_OK, i);
                    finish();
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Download error: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    error.setText(message.toString());
                }
            }

            @Override
            public void onFailure(Call<Reserva> call, Throwable t) {
                progressDialog.dismiss();
                error.setText("Failure in the communication\n" + t.getMessage());
            }
        });
    }
}
