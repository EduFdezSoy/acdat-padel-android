package es.edufdezsoy.reservaspadel.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import es.edufdezsoy.reservaspadel.R;
import es.edufdezsoy.reservaspadel.model.LoginResponse;
import es.edufdezsoy.reservaspadel.network.ApiRestClient;
import es.edufdezsoy.reservaspadel.utils.SharedPreferencesManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static final int REQUEST_REGISTER = 1;

    EditText email, password;
    Button login, register;
    ProgressDialog progressDialog;
    SharedPreferencesManager preferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.input_email);
        password = findViewById(R.id.input_password);
        login = findViewById(R.id.buttonLogin);
        register = findViewById(R.id.buttonRegistro);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate())
                    showMessage("Error al validar");
                else
                    loginByServer();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(LoginActivity.this, RegistroActivity.class), REQUEST_REGISTER);
            }
        });

        preferencesManager = new SharedPreferencesManager(this);
        email.setText(preferencesManager.getEmail());
        password.setText(preferencesManager.getPassword());
    }

    private boolean validate() {
        if (email.getText().toString().isEmpty()) {
            email.setError("El email no puede estar vacio");
            requestFocus(email);

            return false;
        }

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            email.setError("El email no tiene un formato valido");
            requestFocus(email);

            return false;
        }

        if (password.getText().toString().isEmpty()) {
            password.setError("La clave no puede estar vacia");
            requestFocus(password);

            return false;
        }

        return true;
    }

    private void loginByServer() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Iniciando sesion...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        login.setEnabled(false);
        register.setEnabled(false);

        final String mail = email.getText().toString();
        final String pass = password.getText().toString();
//        Log.v("edu", mail);
//        Log.v("edu", pass);
        Call<LoginResponse> call = ApiRestClient.getInstance().login(mail, pass);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                progressDialog.dismiss();

                if (response.isSuccessful()) {
                    preferencesManager.save(mail, pass, response.body().getToken());
                    startActivity(new Intent(getApplicationContext(), ListaActivity.class));
                } else {
                    try {
                        Log.d("edu", response.toString());
                        JSONObject err = new JSONObject(response.errorBody().string());
                        showMessage(err.getString("error"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    }
                }

                login.setEnabled(true);
                register.setEnabled(true);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                showMessage(t.getMessage());

                login.setEnabled(true);
                register.setEnabled(true);
            }
        });
    }

    private void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_REGISTER) {
            if (resultCode == RESULT_OK) {
                //Guardar token y lanzar la lista
                preferencesManager.save(
                        data.getExtras().getString("email"),
                        data.getExtras().getString("password"),
                        data.getExtras().getString("token"));
                startActivity(new Intent(this, ListaActivity.class));
                finish();
            } else if (requestCode == RESULT_CANCELED) {
                //no hacer nada, volver al login
                showMessage("Registro cancelado");
            }
        }
    }
}
