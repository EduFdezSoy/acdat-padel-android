package es.edufdezsoy.reservaspadel.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import es.edufdezsoy.reservaspadel.R;
import es.edufdezsoy.reservaspadel.model.RegisterResponse;
import es.edufdezsoy.reservaspadel.network.ApiRestClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroActivity extends AppCompatActivity {
    EditText name, email, password, rePassword;
    Button register;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        name = findViewById(R.id.input_name);
        email = findViewById(R.id.input_email);
        password = findViewById(R.id.input_password);
        rePassword = findViewById(R.id.input_reEnterPassword);
        register = findViewById(R.id.registerButton);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    register();
                } else {
                    showMessage("registro erroneo");
                }
            }
        });
    }

    private void register() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creando cuenta...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        register.setEnabled(false);

        final String nombre = name.getText().toString();
        final String correo = email.getText().toString();
        final String clave = password.getText().toString();

        Call<RegisterResponse> call = ApiRestClient.getInstance().register(nombre, correo, clave);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                progressDialog.dismiss();
                register.setEnabled(true);

                //onRegisterSuccess();
                if (response.isSuccessful()) {
                    Log.d("onResponse", "" + response.body().getToken());

                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("email", correo );
                    resultIntent.putExtra("password", clave );
                    resultIntent.putExtra("token", response.body().getToken() );
                    setResult(RESULT_OK, resultIntent);
                    finish();
                } else {
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        showMessage(errorObject.getString("error"));
                    } catch (IOException e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                progressDialog.dismiss();
                register.setEnabled(true);

                showMessage(t.getMessage());
            }
        });

    }

    private boolean validate() {
        boolean valid = true;

        if (name.getText().toString().isEmpty() || name.getText().toString().length() < 3) {
            name.setError("Minimo 3 careacteres");
            valid = false;
        } else {
            name.setError(null);
        }

        if (email.getText().toString().isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            email.setError("introduce un correo valido");
            valid = false;
        } else {
            email.setError(null);
        }

        if (password.getText().toString().isEmpty() || password.getText().toString().length() < 4 || password.getText().toString().length() > 10) {
            password.setError("debe tener entre 4 y 10 caracteres");
            valid = false;
        } else {
            password.setError(null);
        }

        if ( !(rePassword.getText().toString().equals(password.getText().toString()))) {
            rePassword.setError("la clave no coincide");
            valid = false;
        } else {
            rePassword.setError(null);
        }

        return valid;
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

}
