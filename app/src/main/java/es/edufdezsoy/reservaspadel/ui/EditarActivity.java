package es.edufdezsoy.reservaspadel.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import es.edufdezsoy.reservaspadel.R;
import es.edufdezsoy.reservaspadel.model.Reserva;
import es.edufdezsoy.reservaspadel.network.ApiTokenRestClient;
import es.edufdezsoy.reservaspadel.utils.SharedPreferencesManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarActivity extends AppCompatActivity {
    TextView error;
    EditText fecha;
    Spinner desde, hasta;
    Button guardar;
    Reserva reserva;
    ProgressDialog progressDialog;
    SharedPreferencesManager preferencesManager;
    SimpleDateFormat dateFormatFrom;
    SimpleDateFormat dateFormatTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        Intent intent = getIntent();
        reserva = (Reserva) intent.getSerializableExtra("reserva");
        preferencesManager = new SharedPreferencesManager(this);
        dateFormatFrom = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatTo = new SimpleDateFormat("dd/MM/yyyy");

        error = findViewById(R.id.textViewError);
        fecha = findViewById(R.id.editTextFecha);
        desde = findViewById(R.id.spinnerDesde);
        hasta = findViewById(R.id.spinnerHasta);
        guardar = findViewById(R.id.button);

        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveClick(v);
            }
        });

        setReserva(reserva);
    }

    private void setReserva(Reserva reserva) {
        // Log.e("err", reserva.getDesde()); // 2019-03-06 24:60:00
        Date date = null;
        try {
            date = dateFormatFrom.parse(reserva.getDesde());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        fecha.setText(dateFormatTo.format(date));

        // 9 es 0 ----  hasta 23
        String time = reserva.getDesde().split(" ")[1];
        time = time.split(":")[0];
        int horas = Integer.parseInt(time);
        desde.setSelection(horas - 9);

        time = reserva.getHasta().split(" ")[1];
        time = time.split(":")[0];
        horas = Integer.parseInt(time);

        hasta.setSelection(horas - 9);
    }

    private void onSaveClick(View v) {
        if (desde.getSelectedItemPosition() > hasta.getSelectedItemPosition()) {
            error.setText("comprueba las horas, no puedes reservar la pista asi");
            return;
        }
        if (hasta.getSelectedItemPosition() > desde.getSelectedItemPosition() + 2) {
            error.setText("solo puedes reservar un maximo de dos horas");
            return;
        }

        Date ahora = new Date();
        Date reservaNueva = null;
        try {
            reservaNueva = dateFormatTo.parse(fecha.getText().toString() + " " + (desde.getSelectedItemPosition() + 9) + ":00");
        } catch (ParseException e) {
            error.setText("Formato de fecha erroneo, usa dd/MM/yyyy");
            return;
        }
        if (reservaNueva.before(ahora)) {
            error.setText("No puedes reservar la pista en el pasado!");
            return;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        reserva.setDesde(simpleDateFormat.format(reservaNueva));
        try {
            reservaNueva =  dateFormatTo.parse(fecha.getText().toString() + " " + (hasta.getSelectedItemPosition() + 9) + ":00");
        } catch (ParseException e) {
            e.printStackTrace();
            error.setText("error inexperado");
            return;
        }
        reserva.setHasta(simpleDateFormat.format(reservaNueva));
        error.setText("");

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Conectando...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<Reserva> call = ApiTokenRestClient.getInstance(preferencesManager.getToken()).updateReserva(reserva, reserva.getId());
        call.enqueue(new Callback<Reserva>() {
            @Override
            public void onResponse(Call<Reserva> call, Response<Reserva> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Reserva reserva1 = response.body();
                    Intent i = new Intent();
                    i.putExtra("reserva", reserva1);
                    setResult(RESULT_OK, i);
                    finish();
                } else {
                    StringBuilder message = new StringBuilder();
                    message.append("Download error: " + response.code());
                    if (response.body() != null)
                        message.append("\n" + response.body());
                    if (response.errorBody() != null)
                        try {
                            message.append("\n" + response.errorBody().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    error.setText(message.toString());
                    Log.e("edu", message.toString());
                }
            }

            @Override
            public void onFailure(Call<Reserva> call, Throwable t) {
                Log.e("edu", t.getMessage());
                error.setText(t.getMessage());
            }
        });
    }
}
