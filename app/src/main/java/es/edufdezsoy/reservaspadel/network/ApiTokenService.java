package es.edufdezsoy.reservaspadel.network;

import java.util.ArrayList;

import es.edufdezsoy.reservaspadel.model.Email;
import es.edufdezsoy.reservaspadel.model.LoginResponse;
import es.edufdezsoy.reservaspadel.model.LogoutResponse;
import es.edufdezsoy.reservaspadel.model.RegisterResponse;
import es.edufdezsoy.reservaspadel.model.Reserva;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApiTokenService {
    @FormUrlEncoded
    @POST("api/register")
    Call<RegisterResponse> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("api/login")
    Call<LoginResponse> login(
            @Field("email") String email,
            @Field("password") String password
    );

    @POST("api/logout")
    Call<LogoutResponse> logout(
            //@Header("Authorization") String token
    );

    @GET("api/reservas")
    Call<ArrayList<Reserva>> getReservas(
            //@Header("Authorization") String token
    );

    @POST("api/reservas")
    Call<Reserva> createReserva(
            //@Header("Authorization") String token,
            @Body Reserva reserva
    );

    @PUT("api/reservas/{id}")
    Call<Reserva> updateReserva(
            //@Header("Authorization") String token,
            @Body Reserva reserva,
            @Path("id") int id
    );

    @DELETE("api/reservas/{id}")
    Call<ResponseBody> deleteReserva(
            //@Header("Authorization") String token,
            @Path("id") int id
    );

    // esto no lo uso pero lo he copiado por si acaso jajaxd
    @POST("api/email")
    Call<ResponseBody> sendEmail(@Body Email email);
}

