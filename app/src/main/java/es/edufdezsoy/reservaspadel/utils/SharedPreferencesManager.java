package es.edufdezsoy.reservaspadel.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager {

    public static final String APP = "MyApp";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String TOKEN = "token";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public SharedPreferencesManager(Context context){
        sharedPreferences = context.getSharedPreferences(APP, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void save (String email, String password, String token){
        editor.putString(EMAIL, email);
        editor.putString(PASSWORD, password);
        editor.putString(TOKEN, token);
        editor.apply();
    }
    public void saveEmail(String key, String value){
        editor.putString(key, value);
        editor.apply();
    }

    public String getEmail(){
        return sharedPreferences.getString(EMAIL, "");
    }

    public void savePassword(String key, String value){
        editor.putString(key, value);
        editor.apply();
    }

    public String getPassword(){
        return sharedPreferences.getString(PASSWORD, "");
    }

    public void saveToken(String key, String value){
        editor.putString(key, value);
        editor.apply();
    }

    public String getToken(){
        return sharedPreferences.getString(TOKEN, "");
    }
}
